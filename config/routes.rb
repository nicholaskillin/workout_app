Rails.application.routes.draw do
  get 'sessions/new'
  root 'sessions#new'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  post '/signup', to: 'users#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users
  resources :workouts, only: [:create, :destroy, :show]
  resources :rounds, only: [:create, :destory,]
  resources :machines, only: [:create]
  resources :users do
    resources :workouts
  end
end
