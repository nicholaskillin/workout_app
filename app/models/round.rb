class Round < ApplicationRecord
    belongs_to :workout
    has_many :machines
    default_scope -> { order(created_at: :asc) }
end
