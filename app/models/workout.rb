class Workout < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :date, presence: true
  has_many :rounds, dependent: :destroy
end
