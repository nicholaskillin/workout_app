class WorkoutsController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    
    def show
        @workout = Workout.find(params[:id])
    end
    
    def index
    end
        
    def create
        @workout = current_user.workouts.build(workout_params)
        if @workout.save
            flash[:success] = "YOU DID IT!!!"
            redirect_to current_user
        else
            flash[:danger] = "Bummer. Try again."
            redirect_to current_user
        end
    end

    def destroy
    end
    
    private
    
        def workout_params
            params.require(:workout).permit(:date)
        end
end
