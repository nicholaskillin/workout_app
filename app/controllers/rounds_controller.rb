class RoundsController < ApplicationController
    
    def create
        @round = current_user.workouts.first.rounds.build
        if @round.save
            flash[:success] = "YOU DID IT!!!"
            redirect_to current_user
        else
            flash[:danger] = "Bummer. Try again."
            redirect_to current_user
        end
    end
    
end
