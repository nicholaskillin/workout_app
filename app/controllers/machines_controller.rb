class MachinesController < ApplicationController
    
    def create
        if params[:machine][:roundnumber] == "2"
            @machine = current_user.workouts.first.rounds.last.machines.build(machine_params)
        else
            @machine = current_user.workouts.first.rounds.first.machines.build(machine_params)
        end
        @machine.save
        redirect_to current_user
    end
    
    
    
    private
    
    def machine_params
        params.require(:machine).permit(:name, :weight, :reps, :roundnumber)
    end
end
