class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @workout = Workout.new
    @round = Round.new
    @machine = Machine.new
    @machine2 = Machine.new
    if current_user.workouts.first.rounds.count == 1 or current_user.workouts.first.rounds.count == 2
      @machine1round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Abdominal Crunch")
      @machine2round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Back Extension")
      @machine3round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Triceps Press")
      @machine4round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Biceps Curl")
      @machine5round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Shoulder Press")
      @machine6round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Lateral Raise")
      @machine7round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Row/Rear Deltoid")
      @machine8round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Chest Press")
      @machine9round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Hip Adduction")
      @machine10round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Hip Abduction")
      @machine11round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Calf Extension")
      @machine12round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Seated Leg Press")
      @machine13round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Seated Leg Curl")
      @machine14round1 = current_user.workouts.first.rounds.first.machines.find_by(name: "Leg Extension")
    end
    if current_user.workouts.first.rounds.count == 2
      @machine1round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Abdominal Crunch")
      @machine2round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Back Extension")
      @machine3round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Triceps Press")
      @machine4round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Biceps Curl")
      @machine5round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Shoulder Press")
      @machine6round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Lateral Raise")
      @machine7round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Row/Rear Deltoid")
      @machine8round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Chest Press")
      @machine9round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Hip Adduction")
      @machine10round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Hip Abduction")
      @machine11round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Calf Extension")
      @machine12round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Seated Leg Press")
      @machine13round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Seated Leg Curl")
      @machine14round2 = current_user.workouts.first.rounds.last.machines.find_by(name: "Leg Extension")
    end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      redirect_to @user
    else
      render 'new'
    end
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
end