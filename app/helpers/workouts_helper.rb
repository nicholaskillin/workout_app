module WorkoutsHelper
    
    def current_workout
        @current_workout ||= Workout.find_by(id: current_user.workouts.first.id )
    end
end
