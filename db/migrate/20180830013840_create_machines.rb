class CreateMachines < ActiveRecord::Migration[5.2]
  def change
    create_table :machines do |t|
      t.string :name
      t.integer :weight
      t.integer :reps
      t.references :round, foreign_key: true

      t.timestamps
    end
  end
end
