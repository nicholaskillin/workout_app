class Add < ActiveRecord::Migration[5.2]
  def change
    add_reference :rounds, :workout, foreign_key: true
  end
end
