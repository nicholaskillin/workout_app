class CreateWorkouts < ActiveRecord::Migration[5.2]
  def change
    create_table :workouts do |t|
      t.date :date
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :workouts, [:user_id, :date]
  end
end
