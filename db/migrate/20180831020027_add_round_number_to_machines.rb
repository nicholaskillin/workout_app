class AddRoundNumberToMachines < ActiveRecord::Migration[5.2]
  def change
    add_column :machines, :roundnumber, :integer
  end
end
